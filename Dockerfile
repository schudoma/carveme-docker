FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="1.2024.02-1"
LABEL description="samestr docker image"


ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev 
RUN apt clean

RUN mkdir -p /opt/software

WORKDIR /opt/software

RUN wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/software/miniconda3 && \
rm -f Miniconda3-latest-Linux-x86_64.sh && \
/opt/software/miniconda3/bin/conda install -y -c conda-forge -c bioconda diamond pyscipopt && \
/opt/software/miniconda3/bin/pip install carveme

WORKDIR /opt/software/miniconda3/lib

RUN ln -s libscip.so.8.1.0.0 libscip.so.8.0

ENV PATH /opt/software/miniconda3/bin:$PATH

WORKDIR /opt/software/miniconda3/lib/python3.12/site-packages/carveme/data/generated

RUN ls -l

RUN /opt/software/miniconda3/bin/diamond makedb --in bigg_proteins.faa -d bigg_proteins



  
